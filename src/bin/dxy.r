use anyhow::Result;
#[cfg(test)]
#[allow(unused_imports)]
use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};

fn solve(input: &str) -> u32 {
    let _ = input;
    unimplemented!()
}

fn main() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/dXY")?;
    let result = solve(&contents);
    println!("Result: {:#?}", result);
    Ok(())
}

#[test]
fn dXY_test_input_p1() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/dXY.part1")?;
    let result = solve(&contents);
    assert_eq!(result, 142);
    Ok(())
}

#[test]
fn dXY_test_input_p2() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/dXY.part2")?;
    let result = solve(&contents);
    assert_eq!(result, 281);
    Ok(())
}

#[test]
fn dXY_real_input() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/dXY")?;
    let _result = solve(&contents);
    Ok(())
}
