use anyhow::Result;
#[cfg(test)]
#[allow(unused_imports)]
use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};

fn part_two(s: &str) -> String {
    // The first and last letter are added to the number, for the case two numbers share one:
    // Correct: eightwothree => 823 => 83
    // Wrong: eightwothree => eigh23 => 23
    s.replace("one", "o1e")
        .replace("two", "t2o")
        .replace("three", "t3e")
        .replace("four", "f4r")
        .replace("five", "f5e")
        .replace("six", "s6x")
        .replace("seven", "s7n")
        .replace("eight", "e8t")
        .replace("nine", "n9e")
}

fn extract_number_from_line(line: &str) -> Option<u32> {
    let replaced_line = part_two(line);
    let mut digits = replaced_line
        .chars()
        .filter_map(|c| c.to_digit(10))
        .map(|v| v as u32);
    let first = digits.next()?;
    let last = digits.last();
    let value = 10 * first + last.unwrap_or(first);
    println!("Found: {} in {}", value, line);
    Some(value)
}

fn solve(input: &str) -> u32 {
    input
        .lines()
        .filter_map(|l| extract_number_from_line(l))
        .sum()
}

fn main() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/d01")?;
    let result = solve(&contents);
    println!("Result: {:#?}", result);
    Ok(())
}

#[test]
fn d01_test_input_p1() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/d01.part1")?;
    let result = solve(&contents);
    assert_eq!(result, 142);
    Ok(())
}

#[test]
fn d01_test_input_p2() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/d01.part2")?;
    let result = solve(&contents);
    assert_eq!(result, 281);
    Ok(())
}

#[test]
fn d01_real_input() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/d01")?;
    let _result = solve(&contents);
    Ok(())
}
