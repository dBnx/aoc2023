use std::{iter::Sum, ops::Add, str::FromStr};

use anyhow::{anyhow, bail, Result};
#[cfg(test)]
#[allow(unused_imports)]
use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};

#[derive(Default, Debug, PartialEq, Copy, Clone)]
struct Draw {
    red: usize,
    green: usize,
    blue: usize,
}

impl Draw {
    fn is_contained_in(&self, limits: &Self) -> bool {
        self.red <= limits.red && self.green <= limits.green && self.blue <= limits.blue
    }

    fn join_self(&mut self, other: &Self) {
        self.red = self.red.max(other.red);
        self.green = self.green.max(other.green);
        self.blue = self.blue.max(other.blue);
    }

    fn join(&self, other: &Self) -> Self {
        let mut copy = self.clone();
        copy.join_self(other);
        copy
    }

    fn power(&self) -> usize {
        self.red * self.green * self.blue
    }
}

impl FromStr for Draw {
    type Err = anyhow::Error;

    /// E.g. 3 blue, 4 red
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut draw: Self = Default::default();
        for set in input.trim().split(',') {
            let (amount, color) = set
                .trim()
                .split_once(' ')
                .ok_or(anyhow!("Invalid format"))?;
            let amount: usize = amount.parse()?;
            match color {
                "red" => draw.red += amount,
                "green" => draw.green += amount,
                "blue" => draw.blue += amount,
                c => bail!("Invalid color {}", c),
            }
        }
        Ok(draw)
    }
}

impl Add<Draw> for Draw {
    type Output = Draw;

    fn add(self, rhs: Self) -> Self::Output {
        Draw {
            red: self.red + rhs.red,
            green: self.green + rhs.green,
            blue: self.blue + rhs.blue,
        }
    }
}

impl Sum for Draw {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        let mut total = Default::default();
        for v in iter {
            total = total + v;
        }
        total
    }
}

#[derive(Default, Debug)]
struct Game {
    id: usize,
    draws: Vec<Draw>,
}

impl Game {
    /// Returns minimal amount of cubes that need to be present for a game to be possible
    fn get_minimal_set(&self) -> Draw {
        self.draws
            .iter()
            .fold(Draw::default(), |acc, e| acc.join(e))
    }

    /// Product of the minimal set of necessary cubes
    fn get_power(&self) -> usize {
        self.get_minimal_set().power()
    }

    /// Checks if the game would be possible with a given amount of cubes
    fn is_possible(&self, limits: &Draw) -> bool {
        let possible = !self
            .draws
            .iter()
            .map(|draw| draw.is_contained_in(limits))
            .any(|v| v == false);
        possible
    }
}

impl FromStr for Game {
    type Err = anyhow::Error;

    fn from_str(record: &str) -> Result<Self, Self::Err> {
        let (game, subsets) = record.split_once(':').unwrap();
        let (_, id) = game.split_once(' ').ok_or(anyhow!("Valid record"))?;
        let id = id.parse()?;
        let draws = subsets
            .split(';')
            .map(|subset| Draw::from_str(subset).ok())
            .collect::<Option<_>>()
            .ok_or(anyhow!("Parsing draws"))?;

        Ok(Game { id, draws })
    }
}

fn solve(input: &str) -> usize {
    let limits = Draw {
        red: 12,
        green: 13,
        blue: 14,
    };

    input
        .lines()
        .filter_map(|line| Game::from_str(line).ok())
        .filter(|game| game.is_possible(&limits))
        .map(|game| game.id)
        .sum()
}

fn solve_p2(input: &str) -> usize {
    input
        .lines()
        .filter_map(|line| Game::from_str(line).ok())
        .map(|game| game.get_power())
        .sum()
}

fn main() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/d02")?;
    let possible = solve(&contents);
    let power = solve_p2(&contents);
    println!("Result: {:#?} possible games - power: {}", possible, power);
    Ok(())
}

#[test]
fn d02_test_input_p1() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/d02.part1")?;
    let result = solve(&contents);
    assert_eq!(result, 8);
    Ok(())
}

#[test]
fn d02_test_input_p2() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/d02.part2")?;
    let result = solve_p2(&contents);
    assert_eq!(result, 2286);
    Ok(())
}

#[test]
fn d02_real_input() -> Result<()> {
    let contents = std::fs::read_to_string("inputs/d02")?;
    let _result = solve(&contents);
    Ok(())
}

#[test]
fn draw_struct() {
    assert_eq!(
        Draw::from_str("3 blue, 4 red").unwrap(),
        Draw {
            red: 4,
            green: 0,
            blue: 3,
        }
    );

    assert_eq!(
        Draw::from_str("1 blue").unwrap(),
        Draw {
            red: 0,
            green: 0,
            blue: 1,
        }
    );

    assert_eq!(
        Draw::from_str("0 red, 1 blue").unwrap(),
        Draw {
            red: 0,
            green: 0,
            blue: 1,
        }
    );
}
